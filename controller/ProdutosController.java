package controller;
import java.util.List;
import java.util.ArrayList;

import model.entity.Produtos;
import model.ProdutosDao;
import model.Database;

public class ProdutosController
{
    private ProdutosDao dao;
    
    public ProdutosController (Database database)
    {
        this.dao = new ProdutosDao(database);
    }
    
    public void create (Produtos produto) throws Exception
    {
        dao.create(produto);
    }
    
    public List<Produtos> readAll() throws Exception
    {
        return dao.readAll();
    }

    public Produtos loadForCodBar(String codBar) throws Exception
    {
        return dao.loadForCodBar(codBar);
    }
    
    public void update (Produtos produto)throws Exception
    {
        dao.update(produto);
    }
    
    public boolean  existCodBar(String codBar) throws Exception
    {
        return dao. existCodBar(codBar);
    }
}
