package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;
import model.entity.Produtos;
import java.util.Date;
import model.entity.Validade;
/**
 * Escreva a descrição da classe ValidadesDao aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class ValidadeDao
{
    public static Database database;
    public static Dao<Validade, Integer> dao;
    private Validade validadeCarregada;
    private List<Validade> validadesCarregados;

    public ValidadeDao (Database database)
    {
        ValidadeDao.setDatabase(database);
        validadesCarregados = new ArrayList<Validade>();
    }

    public static void setDatabase(Database database)
    {
        ValidadeDao.database = database;
        try
        {
            dao = DaoManager.createDao(database.getConnection(), Validade.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Validade.class);
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }

    public void create (Validade validade) throws Exception
    {
        int nrows = 0;
        try
        {
            nrows = dao.create(validade);
            if(nrows == 0)
                throw new SQLException("Erro ao cadastrar o validade");
            this.validadeCarregada = validade;
            validadesCarregados.add (validade);
        }
        catch(SQLException e)
        {
            throw new Exception();
        }
    }   

    public List<Validade> readAll() throws Exception
    {
        try 
        {
            this.validadesCarregados = dao.queryForAll();
        }
        catch(SQLException e)
        {
            throw new Exception();
        }

        return this.validadesCarregados;
    }

    public Validade loadForId(int id) throws Exception
    {
        try 
        {
            return dao.queryForId(id);
        }
        catch(SQLException e)
        {
            throw new Exception();
        }
    }

    
    public void update (Validade validade) throws Exception
    {
        try
        {
            dao.update(validade);
        }   
        catch (java.sql.SQLException e)
        {
            throw new Exception();
        }   
    }

    public int existEquals(Validade validade) throws Exception
    {
        try 
        {
            this.validadesCarregados = dao.queryForAll();
            for( int i = 0; i < validadesCarregados.size(); i++)
            {
                if(validade.getCodBar().equals(validadesCarregados.get(i).getCodBar()))
                { 
                    if (validade.getNome().equals(validadesCarregados.get(i).getNome())) 

                    {               
                        if(validade.getEndDate().equals(validadesCarregados.get(i).getEndDate())) 
                            return validadesCarregados.get(i).getId();;
                    }
                }
            }
            return -1;
        }
        catch(SQLException e)
        {
            throw new Exception();
        }
    }

    

    public void delete (Validade validade) throws Exception
    {
        try 
        {
            dao.deleteById(validade.getId());
        }
        catch (SQLException e)
        {
            throw new Exception(); 
        }
    }  
}