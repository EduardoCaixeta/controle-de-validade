package model.entity;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import java.sql.SQLException;
import java.time.ZoneId;
public class Users
{
    @DatabaseField(generatedId = true)
    int id;
    
    @DatabaseField
    String username;
    
    @DatabaseField
    String password;

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie username*/
    public String getUsername(){
        return this.username;
    }//end method getUsername

    /**SET Method Propertie username*/
    public void setUsername(String username){
        this.username = username;
    }//end method setUsername

    /**GET Method Propertie password*/
    public String getPassword(){
        return this.password;
    }//end method getPassword

    /**SET Method Propertie password*/
    public void setPassword(String password){
        this.password = password;
    }//end method setPassword

    //End GetterSetterExtension Source Code
//!
}
