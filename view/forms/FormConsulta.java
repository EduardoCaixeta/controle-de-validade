package view.forms;

import view.panels.PanelConsulta;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import java.awt.Graphics;
import controller.*;
import model.Database;
import model.entity.*;
import view.panels.utils.TratamentoDate;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.awt.Cursor;
import java.awt.Color;
import view.forms.utils.*;
import view.format.Limite_digitos;
import view.Frame;
public class FormConsulta
{
    private PanelConsulta panel;
    private ValidadeController controllerValidade;
    private List<Validade> validades;
    private Database dbValidade, dbProduto;
    private Frame frame;
    private boolean selectAll;
    public FormConsulta( Database dP, Database dV, Frame frame)
    {
        panel = new PanelConsulta();
        selectAll = false;
        this.frame = frame; 
        this.dbValidade = dV;
        controllerValidade = new ValidadeController(dbValidade);
        try
        {
            validades = controllerValidade.readAll();
            if(validades.size() == 0) 
            {
                 panel.getButtonMarcaAll().setEnabled(false);
                 panel.getButtonDelete().setEnabled(false);
                 panel.getButtonEdit().setEnabled(false);
                throw new Exception("Não há produtos cadastrados.");
            }
        }
        catch(Exception e)
        {
            panel.getTxtStatus().setText(e.getMessage());
        }  
        panel.getButtonAdd().addActionListener(e -> add_Click());
        panel.getButtonAdd().setMnemonic(KeyEvent.VK_S);
        panel.getButtonAdd().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonAdd().doClick();
                    }
                }
            });

        panel.getButtonPesquisa().addActionListener
        (e -> pesquisa_Click());
        panel.getButtonPesquisa().setMnemonic(KeyEvent.VK_S);
        panel.getButtonPesquisa().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonPesquisa().doClick();
                    }
                }
            });

        panel.getButtonDelete().addActionListener
        (e -> delete_Click());
        panel.getButtonDelete().setMnemonic(KeyEvent.VK_S);
        panel.getButtonDelete().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonDelete().doClick();
                    }
                }
            });

        panel.getButtonMarcaAll().addActionListener
        (e -> marcaAll_Click());
        panel.getButtonMarcaAll().setMnemonic(KeyEvent.VK_S);
        panel.getButtonMarcaAll().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonMarcaAll().doClick();
                    }
                }
            });

        panel.getButtonEdit().addActionListener
        (e -> edit_Click());
        panel.getButtonEdit().setMnemonic(KeyEvent.VK_S);
        panel.getButtonEdit().addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        panel.getButtonEdit().doClick();
                    }
                }
            });

        panel.getTxtCodBar().addActionListener(e -> pesquisa_Click());

        panel.getTxtNome().addActionListener(e -> pesquisa_Click());
        panel.getComboLocal().addActionListener(e -> pesquisa_Click());
        menuUpdate();
    }

    public void menuUpdate()
    {
        frame.getMenuBarra().desableMenu();
        frame.getMenuBarra().getButtonConsulta().setContentAreaFilled(true);
        frame.getMenuBarra().getButtonConsulta().setBackground(new Color(102,153,255));
    }
    
    public PanelConsulta getPanel()
    {
        return this.panel;
    }

    public void add_Click()
    {

    }

    public void marcaAll_Click()
    {
        if(selectAll)
        {
            panel.noSelect();
            selectAll = false;
        }
        else
        {
            panel.getTable().selectAll();
            selectAll = true;
        }
    }

    public void altere( Consulta_Alterar editPane)
    {
     Alteracoes alterar = new Alteracoes(editPane, this);  
     alterar.altere();
    }
    
    public void edit_Click()
    {
        try
        {
            panel.getTxtStatus().setText("");
            if(panel.getTable().getSelectedRow() != -1)
            {
                Validade updateValidade = panel.getModelTable().getValidades().get(panel.getTable().getSelectedRow());
                Consulta_Alterar editPane = new Consulta_Alterar(frame, updateValidade){
                    @Override
                    public void save_Click()
                        {
                            altere(this);
                            
                            panel.getModelTable().updateValidade(updateValidade);
                    }
                };
                editPane.showFrame(); }
            else
            {
                throw new Exception("Selecione um cadastro");
            }
        }
        catch(Exception e)
        {
            panel.getTxtStatus().setText(e.getMessage());
        }
    }
      
    
    public void pesquisa_Click()
    {
        panel.getTxtStatus().setText("");

        try
        {
            if((panel.getTxtCodBar().getText().trim().isEmpty() == false
                || panel.getTxtNome().getText().trim().isEmpty() == false)&&
            panel.getComboLocal().getSelectedItem().toString().equals("Todos"))
            {   
                if(panel.getTxtNome().getText().trim().isEmpty())
                {
                    String codBar = panel.getTxtCodBar().getText();
                    List<Validade> validadesCodBar = new ArrayList<Validade>();
                    for( int i =0; i<validades.size(); i++)
                    {
                        if(validades.get(i).getCodBar().equals(codBar))
                        {
                            if(validadesCodBar.add(validades.get(i)));
                            else
                            {
                                panel.getModelTable().setValidades(validades);
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount()
                                    + " Validades cadastradas");
                                throw new Exception("Erro ao buscar.");
                            }
                        }
                    }

                    if(validadesCodBar.size() != 0)
                    {

                        panel.getModelTable().setValidades(validadesCodBar);
                        panel.getTxtCountRows().setText(panel.getTable().getRowCount()
                            + " Validades cadastradas");
                        throw new Exception("Produto encontrado.");
                    }
                    else
                    {
                        throw new Exception("Produto não encontrado.");
                    }
                }
                else
                {
                    String nome = panel.getTxtNome().getText();
                    List<Validade> validadesNomes = new ArrayList<Validade>();
                    for( int i =0; i<validades.size(); i++)
                    {
                        String sub = validades.get(i).getNome().substring(0, nome.length());
                        if(sub.equals(nome))
                        {
                            if(validadesNomes.add(validades.get(i)));
                            else
                            {
                                panel.getModelTable().setValidades(validades);
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                throw new Exception("Erro ao buscar.");
                            }
                        }
                    }
                    if(panel.getTxtCodBar().getText().trim().isEmpty())
                    {
                        if(validadesNomes.size() != 0)
                        {

                            panel.getModelTable().setValidades(validadesNomes);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Produto encontrado.");
                        }
                        else
                        {

                            panel.getModelTable().setValidades(validades);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Produto não encontrado.");
                        }
                    }
                    else
                    {
                        String codBar = panel.getTxtCodBar().getText();
                        List<Validade> validadesCodBar = new ArrayList<Validade>();
                        for( int i = 0; i<validadesNomes.size(); i++)
                        {
                            if(validadesNomes.get(i).getCodBar().equals(codBar))
                            {
                                if(validadesCodBar.add(validadesNomes.get(i)));
                                else
                                {
                                    panel.getModelTable().setValidades(validades);
                                    panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                    throw new Exception("Erro ao buscar.");
                                }
                            }
                        }

                        if(validadesCodBar.size() != 0)
                        {

                            panel.getModelTable().setValidades(validadesCodBar);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Produto encontrado.");
                        }
                        else
                        {
                            panel.getModelTable().setValidades(validades);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Produto não encontrado.");
                        }
                    }
                }
            }
            else
            {
                String local = panel.getComboLocal().getSelectedItem().toString();
                if(panel.getTxtCodBar().getText().trim().isEmpty()
                && panel.getTxtNome().getText().trim().isEmpty())
                {

                    if(local.equals("Todos") == false)
                    {
                        List<Validade> validadesLocal = new ArrayList<Validade>();
                        for( int i = 0; i<validades.size(); i++)
                        {
                            if(validades.get(i).getLocal().equals(local))
                            {
                                if(validadesLocal.add(validades.get(i)));
                                else
                                {   panel.getModelTable().setValidades(validades);
                                    panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                    throw new Exception("Erro ao buscar.");
                                }
                            }
                        }

                        if(validadesLocal.size() != 0)
                        {
                            panel.getModelTable().setValidades(validadesLocal);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Produtos encontrados.");
                        }
                        else
                        {
                            panel.getModelTable().setValidades(validades);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Não há produtos neste local.");
                        }
                    }
                    else
                    {

                        if(validades.size()!=0)
                        {
                            panel.getModelTable().setValidades(validades);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                            throw new Exception("Produtos encontrados.");
                        }
                        else
                        {   

                            throw new Exception("Não há produtos cadastrados.");
                        }
                    }
                }
                else
                {
                    if(panel.getTxtNome().getText().trim().isEmpty())
                    {
                        String codBarLocal = panel.getTxtCodBar().getText();
                        List<Validade> validadesCodBarLocal = new ArrayList<Validade>();
                        for( int i =0; i<validades.size(); i++)
                        {
                            if(validades.get(i).getCodBar().equals(codBarLocal) && validades.get(i).getLocal().equals(local))
                            {
                                if(validadesCodBarLocal.add(validades.get(i)));
                                else
                                {
                                    panel.getModelTable().setValidades(validades);
                                    panel.getTxtCountRows().setText(panel.getTable().getRowCount()
                                        + " Validades cadastradas");
                                    throw new Exception("Erro ao buscar.");
                                }
                            }
                        }

                        if(validadesCodBarLocal.size() != 0)
                        {

                            panel.getModelTable().setValidades(validadesCodBarLocal);
                            panel.getTxtCountRows().setText(panel.getTable().getRowCount()
                                + " Validades cadastradas");
                            throw new Exception("Produto encontrado.");
                        }
                        else
                        {
                            throw new Exception("Produto não encontrado neste local.");
                        }
                    }

                    else
                    {
                        String nomeLocal = panel.getTxtNome().getText();
                        List<Validade> validadesNomesLocal = new ArrayList<Validade>();
                        for( int i =0; i<validades.size(); i++)
                        {
                            String sub = validades.get(i).getNome().substring(0, nomeLocal.length());
                            if(sub.equals(nomeLocal) && validades.get(i).getLocal().equals(local))
                            {
                                if(validadesNomesLocal.add(validades.get(i)));
                                else
                                {
                                    panel.getModelTable().setValidades(validades);
                                    panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                    throw new Exception("Erro ao buscar.");
                                }
                            }
                        }
                        if(panel.getTxtCodBar().getText().trim().isEmpty())
                        {
                            if(validadesNomesLocal.size() != 0)
                            {

                                panel.getModelTable().setValidades(validadesNomesLocal);
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                throw new Exception("Produto encontrado.");
                            }
                            else
                            {

                                panel.getModelTable().setValidades(validades);
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                throw new Exception("Produto não encontrado.");
                            }
                        }
                        else
                        {
                            String codBar = panel.getTxtCodBar().getText();
                            List<Validade> validadesCodBarLocal = new ArrayList<Validade>();
                            for( int i = 0; i<validadesNomesLocal.size(); i++)
                            {
                                if(validadesNomesLocal.get(i).getCodBar().equals(codBar))
                                {
                                    if(validadesCodBarLocal.add(validadesNomesLocal.get(i)));
                                    else
                                    {
                                        panel.getModelTable().setValidades(validades);
                                        panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                        throw new Exception("Erro ao buscar.");
                                    }
                                }
                            }

                            if(validadesCodBarLocal.size() != 0)
                            {

                                panel.getModelTable().setValidades(validadesCodBarLocal);
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                throw new Exception("Produto encontrado.");
                            }
                            else
                            {
                                panel.getModelTable().setValidades(validades);
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                throw new Exception("Produto não encontrado.");
                            }
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            panel.getTxtStatus().setText(e.getMessage());
        }

    }

    public void pesqLocal_Click(String local)
    {
        panel.getTxtStatus().setText("");
        try
        {
            List<Validade> validadesLocal = new ArrayList<Validade>();
            for( int i = 0; i<validades.size(); i++)
            {
                if(validades.get(i).getLocal().equals(local))
                {
                    if(validadesLocal.add(validades.get(i)));
                    else
                    {
                        throw new Exception("Erro ao buscar.");
                    }
                }
            }

            if(validadesLocal.size() != 0)
            {
                panel.getModelTable().setValidades(validadesLocal);
                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                throw new Exception("Produtos encontrados.");
            }
            else
            {
                throw new Exception("Não há produtos neste local.");
            }
        }
        catch(Exception e)
        {
            panel.getTxtStatus().setText(e.getMessage());
        }
    }

    public void delete_Click()
    {
        panel.getTxtStatus().setText("");
        try
        {
            if(panel.getTable().getSelectedRowCount()!= 0)
            {

                List<Validade> selecionados = new ArrayList<Validade>();
                List<Validade> validadesTable = panel.getModelTable().getValidades();
                for(int i = 0; i<panel.getTable().getSelectedRowCount(); i++)
                {
                    int p = panel.getTable().getSelectedRows()[i];
                    selecionados.add(validadesTable.get(p));
                }
                if(selecionados.size()!=0)
                {
                    OptionPaneDelete delete = new OptionPaneDelete(selecionados, frame){
                            @Override
                            public void delete_Click()
                            {
                                removerValidades(selecionados);
                                close();
                                panel.getTxtCountRows().setText(panel.getTable().getRowCount() + " Validades cadastradas");
                                if(validades.size() != 0) panel.noSelect();
                            }

                            @Override
                            public void cancel_Click()
                            {
                                close();
                                panel.noSelect();
                            }
                        };
                }
                else
                {
                    throw new Exception("Selecione um cadastro");
                }
            }
            else
            {
                throw new Exception("Selecione um cadastro");
            }
        }
        catch( Exception e)
        {
            panel.getTxtStatus().setText(e.getMessage());
        }

    }

    public void removerValidades(List<Validade> selecionados) 
    {
        panel.getTxtStatus().setText("");
        try
        {
            for(int i = 0; i < selecionados.size(); i++)
            {
                panel.getModelTable().removeValidade(selecionados.get(i));
                controllerValidade.delete(selecionados.get(i));
            }

            if (panel.getModelTable().getValidades().size() == 0) 
            {
                panel.getButtonMarcaAll().setEnabled(false);
                panel.getButtonDelete().setEnabled(false);
                panel.getButtonEdit().setEnabled(false);
            }

            validades = controllerValidade.readAll();
        }

        catch(Exception e)
        {
            panel.getTxtStatus().setText(e.getMessage());
        }
    }

}
