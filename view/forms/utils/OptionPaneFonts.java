
package view.forms.utils;

import java.awt.Font;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import model.entity.*; 
import javax.swing.ImageIcon;
import java.util.List;
import view.panels.utils.ValidadeTableModel;

import view.panels.PanelCadastro;
public class OptionPaneFonts {

    public int getOptionPaneUpdateProduto(JPanel panel, Produtos produto, Validade validade) {
        Object[] options = {"ATUALIZAR",
                "APENAS CADASTRAR"};
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.PLAIN, 12)));

        String text =
            "Deseja atualizar o produtro?\n\n";
        if(produto.getNome().equals(validade.getNome()) == false)
        {
            text += "\n\nNome atual: "+ 
            produto.getNome()
            +"\nNome novo: "+validade.getNome();
            if(produto.getLocal().equals(validade.getLocal()) == false)
            {
                text += "\n\nLocal atual: "+ 
                produto.getLocal() 
                +"\nLocal novo: "+validade.getLocal()+"\n\n";
                return JOptionPane.showOptionDialog(panel,
                    text,
                    "Atualizar Produto",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    new ImageIcon("edit.png"),     
                    options,  
                    options[0]);
            }
            return JOptionPane.showOptionDialog(panel,
                text,
                "Atualizar Produto",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                new ImageIcon("edit.png"),     
                options,  
                options[0]);
        }
        else
        {
            if(produto.getLocal().equals(validade.getLocal()) == false)
            {
                text += "\n\nLocal atual: "+ 
                produto.getLocal() 
                +"\nLocal novo: "+validade.getLocal()+"\n\n";
                return JOptionPane.showOptionDialog(panel,
                    text,
                    "Atualizar Produto",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    new ImageIcon("edit.png"),     
                    options,  
                    options[0]);
            }    
            else 
            {
                return JOptionPane.showOptionDialog(panel,
                    text,
                    "Atualizar Produto",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    new ImageIcon("edit.png"),     
                    options,  
                    options[0]);
            }

        }
    }

    public int getOptionPaneEdit(JPanel panel, Validade oldValidade, Validade newValidade)
    {
        Object[] options = {"ATUALIZAR AMBOS",
                "APENAS A VALIDADE"};
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.PLAIN, 12))); 

        String text =  "Deseja atualizar o produtro?";
        if(oldValidade.getCodBar().equals(newValidade.getCodBar())==false)
        {
            text += "\n\nCódigo de Barras atual: "+oldValidade.getCodBar()+
            "\nCódigo de Barras novo: "+newValidade.getCodBar();

            if(oldValidade.getNome().equals(newValidade.getNome()) == false)
            {
                text += "\n\nNome atual: "+ 
                oldValidade.getNome()
                +"\nNome novo: "+newValidade.getNome();
                if(oldValidade.getLocal().equals(newValidade.getLocal()) == false)
                {
                    text += "\n\nLocal atual: "+ 
                    oldValidade.getLocal() 
                    +"\nLocal novo: "+newValidade.getLocal()+"\n\n";
                    return JOptionPane.showOptionDialog(panel,
                        text,
                        "Atualizar Produto",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon("edit.png"),     
                        options,  
                        options[0]);
                }
                return JOptionPane.showOptionDialog(panel,
                    text,
                    "Atualizar Produto",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    new ImageIcon("edit.png"),     
                    options,  
                    options[0]);
            }
            else
            {
                if(oldValidade.getLocal().equals(newValidade.getLocal()) == false)
                {
                    text += "\n\nLocal atual: "+ 
                    oldValidade.getLocal() 
                    +"\nLocal novo: "+newValidade.getLocal()+"\n\n";
                    return JOptionPane.showOptionDialog(panel,
                        text,
                        "Atualizar Produto",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon("edit.png"),     
                        options,  
                        options[0]);
                }    
                else 
                {
                    return JOptionPane.showOptionDialog(panel,
                        text,
                        "Atualizar Produto",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon("edit.png"),     
                        options,  
                        options[0]);
                }
            }
        }
        else
        {
            if(oldValidade.getNome().equals(newValidade.getNome()) == false)
            {
                text += "\n\nNome atual: "+ 
                oldValidade.getNome()
                +"\nNome novo: "+newValidade.getNome();
                if(oldValidade.getLocal().equals(newValidade.getLocal()) == false)
                {
                    text += "\n\nLocal atual: "+ 
                    oldValidade.getLocal() 
                    +"\nLocal novo: "+newValidade.getLocal()+"\n\n";
                    return JOptionPane.showOptionDialog(panel,
                        text,
                        "Atualizar Produto",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon("edit.png"),     
                        options,  
                        options[0]);
                }
                return JOptionPane.showOptionDialog(panel,
                    text,
                    "Atualizar Produto",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    new ImageIcon("edit.png"),     
                    options,  
                    options[0]);
            }
            else
            {
                if(oldValidade.getLocal().equals(newValidade.getLocal()) == false)
                {
                    text += "\n\nLocal atual: "+ 
                    oldValidade.getLocal() 
                    +"\nLocal novo: "+newValidade.getLocal()+"\n\n";
                    return JOptionPane.showOptionDialog(panel,
                        text,
                        "Atualizar Produto",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon("edit.png"),     
                        options,  
                        options[0]);
                }    
                else 
                {
                    if(oldValidade.printEndDate().equals(newValidade.printEndDate())==false) 
                    {
                        int u =JOptionPane.CANCEL_OPTION;
                        return u;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }
    }

    public int getOptionPaneExistProduto(JPanel panel, Produtos existProduto)
    {
        String text = "O Código de Barra " + existProduto.getCodBar()+" já está relacionado com o produto: "+"\n\nNome: "+ 
            existProduto.getNome() 
            +"\nLocal: "+existProduto.getLocal()+". \n\nDeseja ignorar o código de barras ou altera-lo"+
            " (isto fará com que o produto já cadastrado seja excluído)" ;
        Object[] options = {"ALTERAR PRODUTO",
                "IGNORAR CÓDIGO DE BARRAS"};
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Arial", Font.PLAIN, 12))); 

        return JOptionPane.showOptionDialog(panel,
            text,
            "Atualizar Produto",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            new ImageIcon("edit.png"),     
            options,  
            options[0]);
    }
}