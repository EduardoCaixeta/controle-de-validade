package view.forms.utils;

    import javax.swing.*;
    import javax.swing.text.MaskFormatter;
    import java.awt.Cursor;
    import java.awt.Color;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import java.text.ParseException;
    import java.awt.Font;
import java.awt.Graphics;
import model.entity.Validade;
import view.format.Limite_digitos;
import model.entity.Produtos;
import view.panels.utils.UpperCaseField_Limitado;
import controller.ProdutosController;
import model.Database;
import java.util.Calendar;
public class Consulta_Alterar extends JPanel {

    /**
     * Creates new form ConsultaDetalhes
     */
    public Consulta_Alterar(JFrame bloq, Validade validade)
    {
        this.updateValidade = validade;
        
        this.bloq = bloq;
        initComponents();
        setValores();
    }

    public Date stringToDate( String string ) throws Exception {
        Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
        Matcher matcher = dataPadrao.matcher(string);
        if (matcher.matches()) {
            int dia = Integer.parseInt(matcher.group(1));
            int mes = Integer.parseInt(matcher.group(2));
            int ano = Integer.parseInt(matcher.group(3));
            return (new GregorianCalendar(ano,mes,dia)).getTime();
        }
        else throw new Exception(" Data final inválida.");        
    } 
    
    public void setValores()
    {
        txtCodBar.setText(updateValidade.getCodBar());
        txtNome.setText(updateValidade.getNome());
        txtEndDate.setText(updateValidade.printEndDate());
        comboLocal.setSelectedItem(updateValidade.getLocal());
    }
    
    public void paintComponent(Graphics g){
        g.setColor(new Color(190, 190, 190));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

    }
    
    public void showFrame()
    {
        optionPane = new JDialog(bloq);
        optionPane.add(this);
        optionPane.setSize(315, 290);
        optionPane.setModal(true);
        optionPane.setLocationRelativeTo(bloq);
        optionPane.setLocation(optionPane.getWidth()/2, optionPane.getHeight()/2);
        optionPane.setVisible(true);
    }
           
    public void close()
    {
        optionPane.dispose();
    }
    
    
    public void codBar_Enter (String codBar)
    {
        txtStatus.setText("");
        if(codBar.isEmpty() == false)
        {
            pesquisa_Click(codBar);
            txtCodBar.setBorder(null);
            if(txtNome.getText().isEmpty() == false)
            {
                txtNome.setBorder(null);  
                txtEndDate.requestFocus();
            }
            
        }
        else txtCodBar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
    }

    public void nome_Enter()
    {
        txtStatus.setText("");
        if(txtNome.getText().isEmpty() == false)
        {
            txtNome.setBorder(null);  
            txtEndDate.requestFocus();
        }
        else txtNome.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
    }

    public void endDate_Enter()
    {
        txtStatus.setText("");
        if(txtEndDate.getText().isEmpty() == false)
        {
            try
            {
                checkEndDate();
                comboLocal.requestFocus();
            }
            catch(Exception e)
            {
                txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                txtStatus.setText(e.getMessage());
            }
        }
        else 
        {
            txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
            txtStatus.setText(" Insira a Data de Validade.");
        }
    }
    
    public void pesquisa_Click(String codBar)
    {
       txtStatus.setText("");
        try
        {
           txtEndDate.setBorder(null);
            if(txtCodBar.getText().trim().isEmpty() == false)
            {
                try
                {
                    Produtos produto = new ProdutosController(new Database("Produtos.db")).loadForCodBar(codBar);
                   txtNome.setText(produto.getNome());
                    comboLocal.setSelectedItem(produto.getLocal());
                }
                catch(Exception e)
                {
                    txtNome.setText("");
                   txtNome.requestFocus(); 
                    throw new Exception(e.getMessage());
                }

            }
            else
            {
               txtCodBar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                throw new Exception("Insira o códido de barras");
            }
        }
        catch(Exception e)
        {
            txtStatus.setText(e.getMessage());
        }
    }
     
    public void checkEndDate() throws Exception
    {
        txtEndDate.setBorder(null);
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(stringToDate(txtEndDate.getText()));

        try
        {
            Calendar rgCalendar = Calendar.getInstance();
            endCalendar.add(Calendar.MONTH, -1);
            analiseEndDate(txtEndDate.getText());
            if(endCalendar.before(rgCalendar))
            {
                throw new Exception(" Data Inválida");
            }
        }catch(Exception e)
        {
            txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
            throw new Exception(e.getMessage());
        }
    }
    
    public void analiseEndDate(String text) throws Exception
    {
        try
        {
            int dia = Integer.parseInt(text.substring(0,2));
            int mes = Integer.parseInt(text.substring(3,5));
            int ano = Integer.parseInt(text.substring(6,10));

            int[] meses31 = {4, 6, 9, 11};

            if(1 > dia || dia > 31) throw new Exception("Dia inválido");

            if(1 > mes || mes >12) throw new Exception("Mês inválido");
            if(dia == 31) 
            {
                for (int i =0; i<4; i++)
                {
                    if(mes == meses31[i])
                    {
                        throw new Exception("Dia inválido");
                    }
                }
            }
            if(mes == 2)
            {
                boolean ehBissexto = false;
                if ((ano % 400)==0)
                {
                    ehBissexto = true;
                }else
                {
                    if(((ano % 4)==0) && ((ano % 100)!=0))
                    {
                        ehBissexto = true;
                    }
                    
                }

                if(ehBissexto)
                {
                    if(dia > 29) throw new Exception("Dia inválido");
                }
                else
                {
                    if(dia > 28) throw new Exception("Dia inválido");
                }
            }
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }   

    }
    
    public void  cancel_Click(){}
    
    public void save_Click(){}
    
    private void initComponents() {

        labelCodBar = new JLabel();
        labelCodBar.setFont( new Font("Arial", Font.PLAIN, 12));
        labelNome = new JLabel();
        labelNome.setFont( new Font("Arial", Font.PLAIN, 12));
        
        labelEndDate = new JLabel();
        labelEndDate.setFont( new Font("Arial", Font.PLAIN, 12));
        labelLocal = new JLabel();
        labelLocal.setFont( new Font("Arial", Font.PLAIN, 12));
        txtCodBar = new JTextField();
        txtCodBar.addActionListener(e -> codBar_Enter(txtCodBar.getText()));
        txtCodBar.setBorder(null);
        txtCodBar.setFont( new Font("Arial", Font.PLAIN, 12));
        txtStatus = new JTextField();
        txtStatus.setFont( new Font("Arial", Font.PLAIN, 12));
        txtStatus.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtStatus.setBorder(null);
        txtStatus.setOpaque(false);
        txtNome =  new UpperCaseField_Limitado(50, "[^0-9||^a-z||^A-Z||[ ]]"); try
        {
            txtEndDate = new JFormattedTextField(new MaskFormatter("##/##/####")){
                public void setEditable(boolean edit)
                {
                    super.setEditable(edit);
                    if(edit) super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
                    else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }

            };
        }
        catch(ParseException e)
        {
            txtEndDate = new JTextField();
            txtEndDate.setDocument( new Limite_digitos(10,"[^0-9|[/]]"));
        }
        txtEndDate.setFont( new Font("Arial", Font.PLAIN, 12));
        txtEndDate.addActionListener(e -> endDate_Enter());
        txtEndDate.setBorder(null);
        txtNome.setFont( new Font("Arial", Font.PLAIN, 12));
        txtNome.addActionListener(e -> nome_Enter());
        txtNome.setBorder(null);
        comboLocal = new JComboBox<>();
        comboLocal.setFont( new Font("Arial", Font.PLAIN, 12));
        buttonSave = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        };
        buttonSave.setFocusPainted(false);
        buttonSave.setFont( new Font("Arial", Font.PLAIN, 12));
        buttonSave.addActionListener
        (e -> save_Click());
        
        buttonCancel = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        buttonCancel.addActionListener(e -> cancel_Click());
buttonCancel.setFont( new Font("Arial", Font.PLAIN, 12));
buttonCancel.setFocusPainted(false);
        labelCodBar.setText("Cód. Barras:");

        labelNome.setText("Nome:");

        labelEndDate.setText("Data de Validade:");

        labelLocal.setText("Local:");

        comboLocal.setModel(new DefaultComboBoxModel<>(new String[] { "Corredor 1", "Corredor 2", "Corredor 3", "Frente", "Laticínios" }));

        buttonSave.setText("SALVAR");

        buttonCancel.setText("CANCELAR");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelCodBar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCodBar))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelNome)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNome))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(labelEndDate)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(labelLocal)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(comboLocal, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(buttonSave)
                                                .addGap(14, 14, 14)))
                                        .addGap(18, 18, 18)
                                        .addComponent(buttonCancel)))
                                .addGap(0, 30, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelCodBar)
                    .addComponent(txtCodBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNome)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEndDate)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelLocal)
                    .addComponent(comboLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSave)
                    .addComponent(buttonCancel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>                        
                                         


    // Variables declaration - do not modify
    private Validade updateValidade;
    private JDialog optionPane;
    private JFrame bloq;
    private JButton buttonSave;
    private JButton buttonCancel;
    private JComboBox<String> comboLocal;
    private JLabel labelCodBar;
    private JLabel labelEndDate;
    private JLabel labelLocal;
    private JLabel labelNome;
    private JTextField txtCodBar;
    private JTextField txtEndDate;
    private JTextField txtNome;
    private JTextField txtStatus;
    // End of variables declaration                   

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie updateValidade*/
    public Validade getUpdateValidade(){
        return this.updateValidade;
    }//end method getUpdateValidade

    /**SET Method Propertie updateValidade*/
    public void setUpdateValidade(Validade updateValidade){
        this.updateValidade = updateValidade;
    }//end method setUpdateValidade

    /**GET Method Propertie optionPane*/
    public JDialog getOptionPane(){
        return this.optionPane;
    }//end method getOptionPane

    /**SET Method Propertie optionPane*/
    public void setOptionPane(JDialog optionPane){
        this.optionPane = optionPane;
    }//end method setOptionPane

    /**GET Method Propertie bloq*/
    public JFrame getBloq(){
        return this.bloq;
    }//end method getBloq

    /**SET Method Propertie bloq*/
    public void setBloq(JFrame bloq){
        this.bloq = bloq;
    }//end method setBloq

    /**GET Method Propertie buttonSave*/
    public JButton getButtonSave(){
        return this.buttonSave;
    }//end method getButtonSave

    /**SET Method Propertie buttonSave*/
    public void setButtonSave(JButton buttonSave){
        this.buttonSave = buttonSave;
    }//end method setButtonSave

    /**GET Method Propertie buttonCancel*/
    public JButton getButtonCancel(){
        return this.buttonCancel;
    }//end method getButtonCancel

    /**SET Method Propertie buttonCancel*/
    public void setButtonCancel(JButton buttonCancel){
        this.buttonCancel = buttonCancel;
    }//end method setButtonCancel

    /**GET Method Propertie comboLocal*/
    public javax.swing.JComboBox<java.lang.String> getComboLocal(){
        return this.comboLocal;
    }//end method getComboLocal

    /**SET Method Propertie comboLocal*/
    public void setComboLocal(javax.swing.JComboBox<java.lang.String> comboLocal){
        this.comboLocal = comboLocal;
    }//end method setComboLocal

    /**GET Method Propertie labelCodBar*/
    public JLabel getLabelCodBar(){
        return this.labelCodBar;
    }//end method getLabelCodBar

    /**SET Method Propertie labelCodBar*/
    public void setLabelCodBar(JLabel labelCodBar){
        this.labelCodBar = labelCodBar;
    }//end method setLabelCodBar

    /**GET Method Propertie labelEndDate*/
    public JLabel getLabelEndDate(){
        return this.labelEndDate;
    }//end method getLabelEndDate

    /**SET Method Propertie labelEndDate*/
    public void setLabelEndDate(JLabel labelEndDate){
        this.labelEndDate = labelEndDate;
    }//end method setLabelEndDate

    /**GET Method Propertie labelLocal*/
    public JLabel getLabelLocal(){
        return this.labelLocal;
    }//end method getLabelLocal

    /**SET Method Propertie labelLocal*/
    public void setLabelLocal(JLabel labelLocal){
        this.labelLocal = labelLocal;
    }//end method setLabelLocal

    /**GET Method Propertie labelNome*/
    public JLabel getLabelNome(){
        return this.labelNome;
    }//end method getLabelNome

    /**SET Method Propertie labelNome*/
    public void setLabelNome(JLabel labelNome){
        this.labelNome = labelNome;
    }//end method setLabelNome

    /**GET Method Propertie txtCodBar*/
    public JTextField getTxtCodBar(){
        return this.txtCodBar;
    }//end method getTxtCodBar

    /**SET Method Propertie txtCodBar*/
    public void setTxtCodBar(JTextField txtCodBar){
        this.txtCodBar = txtCodBar;
    }//end method setTxtCodBar

    /**GET Method Propertie txtEndDate*/
    public JTextField getTxtEndDate(){
        return this.txtEndDate;
    }//end method getTxtEndDate

    /**SET Method Propertie txtEndDate*/
    public void setTxtEndDate(JTextField txtEndDate){
        this.txtEndDate = txtEndDate;
    }//end method setTxtEndDate

    /**GET Method Propertie txtNome*/
    public JTextField getTxtNome(){
        return this.txtNome;
    }//end method getTxtNome

    /**SET Method Propertie txtNome*/
    public void setTxtNome(JTextField txtNome){
        this.txtNome = txtNome;
    }//end method setTxtNome

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie txtStatus*/
    public JTextField getTxtStatus(){
        return this.txtStatus;
    }//end method getTxtStatus

    /**SET Method Propertie txtStatus*/
    public void setTxtStatus(JTextField txtStatus){
        this.txtStatus = txtStatus;
    }//end method setTxtStatus

    //End GetterSetterExtension Source Code
//!
}
