package view.forms.utils;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
import javax.swing.BorderFactory;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.List;
import java.awt.Color;
import java.awt. Dimension;
import java.awt.Component;
import java.awt.Font;
import view.panels.PanelCadastro;
import model.entity.Validade;
public class OptionPaneDelete extends JPanel {

    public OptionPaneDelete( List<Validade> selecionados, JFrame bloq) {
        this.selecionados = selecionados;
        initComponents();
        this.bloq = bloq;
        showFrame();
    }

    public void showFrame()
    {
        optionPane = new JDialog(bloq);
        optionPane.add(this);
        optionPane.setSize(340, 260);
        optionPane.setModal(true);
        optionPane.setLocationRelativeTo(bloq);
        optionPane.setLocation(optionPane.getWidth()/2, optionPane.getHeight()/2);
        optionPane.setVisible(true);
    }

    public void close()
    {
        optionPane.dispose();
    }

    private void initComponents() {

        buttonDelete = new JButton();
        buttonDelete.addActionListener(e -> delete_Click());
        buttonCancel = new JButton();
        buttonCancel.addActionListener(e -> cancel_Click());
        jScrollPane1 = new JScrollPane();
        tabel = new JTable();
        linha1 = new JTextField("Deseja realmente deletar ");
        ico = new JLabel(new ImageIcon("delete.png"));
        if(selecionados.size() == 1)
        {
            linha1.setText(linha1.getText() + selecionados.size()+" cadastro?");
        }
        else
        {
            linha1.setText(linha1.getText() + selecionados.size()+" cadastros?");
        }
        linha1.setFont(new Font("Arial", Font.PLAIN, 13));
        linha1.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.BLACK));
        buttonDelete.setText("DELETAR");

        buttonCancel.setText("CANCELAR");

        jScrollPane1.setEnabled(false);
        jScrollPane1.setOpaque(false);
        modelTabel = new DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Local", "Validade"
            }
        );
        for(int i = 0; i < selecionados.size(); i++)
        {
            modelTabel.addRow(new Object[]{selecionados.get(i).getNome(),
                    selecionados.get(i).getLocal(),selecionados.get(i).printEndDate()});
        }

        tabel.setModel(modelTabel);
        tabel.setShowHorizontalLines(false);
        tabel.setShowVerticalLines(false);

        tabel.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
                public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {
                    super.getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);

                    if (row%2 == 0) {

                        setBackground(null);
                    } else {
                        setBackground(new Color(214,214,214));
                    }
                    return this;}});
        tabel.setIntercellSpacing(new Dimension(0,0));
        tabel.setTableHeader(null);
        tabel.setEnabled(false);
        tabel.getColumnModel().getColumn(0).setPreferredWidth(180);
        jScrollPane1.setViewportView(tabel);

        linha1.setEditable(false);


        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addComponent(buttonDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonCancel)
                        .addGap(55, 55, 55))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(ico)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(linha1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ico, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(linha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonDelete)
                    .addComponent(buttonCancel))
                .addContainerGap(25, Short.MAX_VALUE))
        );
    }// </editor-fold>                        

    // Variables declaration - do not modify                     
    private JButton buttonCancel;
    private JButton buttonDelete;
    private JScrollPane jScrollPane1;
    private JTable tabel;
    private JTextField linha1;
    private DefaultTableModel modelTabel;
    private JFrame bloq;
    private JDialog optionPane;
    private List<Validade> selecionados;
    private JLabel ico;

    public void delete_Click(){};

    public void cancel_Click(){};
    // End of variables declaration                   

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie buttonCancel*/
    public JButton getButtonCancel(){
        return this.buttonCancel;
    }//end method getButtonCancel

    /**SET Method Propertie buttonCancel*/
    public void setButtonCancel(JButton buttonCancel){
        this.buttonCancel = buttonCancel;
    }//end method setButtonCancel

    /**GET Method Propertie buttonDelete*/
    public JButton getButtonDelete(){
        return this.buttonDelete;
    }//end method getButtonDelete

    /**SET Method Propertie buttonDelete*/
    public void setButtonDelete(JButton buttonDelete){
        this.buttonDelete = buttonDelete;
    }//end method setButtonDelete

    /**GET Method Propertie jScrollPane1*/
    public JScrollPane getJScrollPane1(){
        return this.jScrollPane1;
    }//end method getJScrollPane1

    /**SET Method Propertie jScrollPane1*/
    public void setJScrollPane1(JScrollPane jScrollPane1){
        this.jScrollPane1 = jScrollPane1;
    }//end method setJScrollPane1

    /**GET Method Propertie tabel*/
    public JTable getJTable1(){
        return this.tabel;
    }//end method getJTable1

    /**SET Method Propertie tabel*/
    public void setJTable1(JTable tabel){
        this.tabel = tabel;
    }//end method setJTable1

    /**GET Method Propertie linha1*/
    public JTextField getLinha1(){
        return this.linha1;
    }//end method getLinha1

    /**SET Method Propertie linha1*/
    public void setLinha1(JTextField linha1){
        this.linha1 = linha1;
    }//end method setLinha1

    /**GET Method Propertie modelTabel*/
    public DefaultTableModel getModelTabel(){
        return this.modelTabel;
    }//end method getModelTabel

    /**SET Method Propertie modelTabel*/
    public void setModelTabel(DefaultTableModel modelTabel){
        this.modelTabel = modelTabel;
    }//end method setModelTabel

    /**GET Method Propertie selecionados*/
    public java.util.List<model.entity.Validade> getSelecionados(){
        return this.selecionados;
    }//end method getSelecionados

    /**SET Method Propertie selecionados*/
    public void setSelecionados(java.util.List<model.entity.Validade> selecionados){
        this.selecionados = selecionados;
    }//end method setSelecionados

    //End GetterSetterExtension Source Code
    //!
}
