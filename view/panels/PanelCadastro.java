
package view.panels;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import java.awt.Cursor;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.GregorianCalendar;
import java.text.ParseException;
import model.entity.Validade;
import view.format.*;
import view.panels.utils.UpperCaseField_Limitado;
import java.awt.Graphics;
/**
 * Escreva a descrição da classe few aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class PanelCadastro extends JPanel {

    
    public PanelCadastro() {
        initComponents();
    }

    public void paintComponent(Graphics g){
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

    }
    
    private void initComponents() {
        buttonAdd = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        buttonDelete = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        buttonCancel = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
               
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        buttonEdit = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        buttonConfirm =  new JButton();
        labelCodBar = new JLabel();
        labelNome = new JLabel();
        labelEndDate = new JLabel();
        labelLocal = new JLabel();
        try
        {
            txtEndDate = new JFormattedTextField(new MaskFormatter("##/##/####")){
                public void setEditable(boolean edit)
                {
                    super.setEditable(edit);
                    if(edit) super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
                    else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }

            };
        }
        catch(ParseException e)
        {
            txtEndDate = new JTextField();
            txtEndDate.setDocument( new Limite_digitos(10,"[^0-9|[/]]"));
        }
        txtCodBar = new JTextField(){
            public void setEditable(boolean edit)
            {
                super.setEditable(edit);
                if(edit) super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        txtCodBar.setDocument( new Limite_digitos(20,"[^0-9]"));
        modelTable = new DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Local", "Validade"
            }
        ){

            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        txtNome = new UpperCaseField_Limitado(50,"[^a-z|^A-Z|^0-9|[ ]]"){
            public void setEditable(boolean edit)
            {
                super.setEditable(edit);
                if(edit) super.setCursor(new Cursor(Cursor.TEXT_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };

        comboLocal = new JComboBox<>(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        comboLocal.setEditable(false);
        buttonPesquisa = new JButton(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        labelTitulo = new JLabel();
        scrollPane = new JScrollPane();
        table = new JTable(){
            public void setEnabled(boolean edit)
            {
                super.setEnabled(edit);
                if(edit) super.setCursor(new Cursor(Cursor.HAND_CURSOR));
                else super.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }

        };
        txtStatus = new JTextField();

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        setFocusCycleRoot(true);
        setOpaque(false);
        buttonConfirm.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonConfirm.setText("Confirmar");
        buttonConfirm.setDisabledIcon(null);
        buttonConfirm.setFocusCycleRoot(true);

        buttonAdd.setFont(new Font("Arial", 0, 12)); // NOI18N
        buttonAdd.setIcon(new ImageIcon("add.png")); // NOI18N
        buttonAdd.setBorder(null);
        buttonAdd.setContentAreaFilled(false);
        buttonAdd.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonAdd.setDisabledIcon(null);
        buttonAdd.setFocusCycleRoot(true);

        buttonDelete.setFont(new Font("Arial", 0, 12)); // NOI18N
        buttonDelete.setIcon(new ImageIcon("delete.png")); // NOI18N
        buttonDelete.setBorder(null);
        buttonDelete.setContentAreaFilled(false);
        buttonDelete.setDisabledIcon(null);
        buttonDelete.setFocusCycleRoot(true);

        buttonCancel.setFont(new Font("Arial", 0, 12)); // NOI18N
        buttonCancel.setIcon(new ImageIcon("Cancelar.png")); // NOI18N
        buttonCancel.setBorder(null);
        buttonCancel.setContentAreaFilled(false);
        buttonCancel.setDisabledIcon(null);
        buttonCancel.setFocusCycleRoot(true);

        buttonEdit.setFont(new Font("Arial", 0, 12)); // NOI18N
        buttonEdit.setIcon(new ImageIcon("edit.png")); // NOI18N
        buttonEdit.setBorder(null);
        buttonEdit.setContentAreaFilled(false);
        buttonEdit.setDisabledIcon(null);

        labelCodBar.setFont(new Font("Arial", 0, 12)); // NOI18N
        labelCodBar.setText("Código de Barras:");
        labelCodBar.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        labelNome.setFont(new Font("Arial", 0, 12)); // NOI18N
        labelNome.setText("Nome:");
        labelNome.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        labelEndDate.setFont(new Font("Arial", 0, 12)); // NOI18N
        labelEndDate.setText("Data de Validade:");
        labelEndDate.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        labelLocal.setFont(new Font("Arial", 0, 12)); // NOI18N
        labelLocal.setText("Local:");
        labelLocal.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        txtEndDate.setBackground(new Color(255,255,255));
        txtCodBar.setFont(new Font("Arial", 0, 12)); // NOI18N
txtCodBar.setBackground(new Color(255,255,255));

        txtNome.setFont(new Font("Arial", 0, 12)); // NOI18N
txtNome.setBackground(new Color(255,255,255));
        comboLocal.setFont(new Font("Arial", 0, 12)); // NOI18N
        comboLocal.setModel(new DefaultComboBoxModel<>(new String[] { "Corredor 1", "Corredor 2", "Corredor 3", "Frente", "Laticínios" }));
comboLocal.setBackground(new Color(255,255,255));
        buttonPesquisa.setFont(new Font("Arial", 0, 12)); // NOI18N
        buttonPesquisa.setIcon(new ImageIcon("pesquisa.png")); // NOI18N
        buttonPesquisa.setBorder(null);
        buttonPesquisa.setContentAreaFilled(false);
        buttonPesquisa.setDisabledIcon(null);
        buttonPesquisa.setFocusCycleRoot(true);

        txtEndDate.setFont(new Font("Arial", 0, 12));

        labelTitulo.setFont(new Font("Arial", 0, 18)); // NOI18N
        labelTitulo.setText("CADASTRO DE PRODUTOS");
        labelTitulo.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        scrollPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        scrollPane.setViewportView(null);

        table.setModel(modelTable);
        table.getColumnModel().getColumn(0).setPreferredWidth(240);
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setFocusable(false);
        table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
                public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {
                    super.getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);

                    if (row%2 == 0) {
                        if(isSelected)
                            setBackground(new Color(225,243,254));
                        else
                            setBackground(null);
                    } else {
                        if(isSelected)
                        {
                            setBackground(new Color(164,220,252));
                        }
                        else
                            setBackground(new Color(214,214,214));
                    }
                    return this;}});
        table.setSelectionForeground(Color.BLACK);
        table.setIntercellSpacing(new Dimension(0,0));
        scrollPane.setViewportView(table);
        txtStatus.setEditable(false);
        txtStatus.setOpaque(false);
        setPreferredSize(new Dimension(650, 500));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelLocal)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboLocal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(buttonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(buttonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(buttonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(labelEndDate)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(labelNome)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(labelCodBar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCodBar, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, 0)
                                .addComponent(buttonPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)

                        .addComponent(txtStatus, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(97, 97, 97)
                        .addComponent(buttonConfirm)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                .addGap(4, 4, 4))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(labelTitulo)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(labelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scrollPane)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonCancel)
                            .addComponent(buttonEdit)
                            .addComponent(buttonDelete)
                            .addComponent(buttonAdd))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(labelCodBar)
                                .addComponent(txtCodBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelNome)
                            .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelEndDate)
                            .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelLocal)
                            .addComponent(comboLocal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addComponent(buttonConfirm)
                        .addGap(5, 5, 5)

                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        txtStatus.setEditable(false);
        txtStatus.setBorder(null);
        txtCodBar.setBorder(null);
        txtNome.setBorder(null);
        txtEndDate.setBorder(null);

        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed (false);
        table.setCellEditor(null);
        table.setRowSelectionAllowed(true);
    }// </editor-fold>                       

    public void noSelect()
    {
        table.clearSelection();
    }

    private Date stringToDate( String string ) throws Exception {
        Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
        Matcher matcher = dataPadrao.matcher(string);
        if (matcher.matches()) {
            int dia = Integer.parseInt(matcher.group(1));
            int mes = Integer.parseInt(matcher.group(2));
            int ano = Integer.parseInt(matcher.group(3));
            return (new GregorianCalendar(ano,mes,dia)).getTime();
        }
        else throw new Exception(" Data final inválida.");        
    } 

    // Variables declaration - do not modify                     
    private JButton buttonAdd;
    private JButton buttonDelete;
    private JButton buttonCancel;
    private JButton buttonEdit;
    private JButton buttonPesquisa;
    private JButton buttonConfirm;
    private JComboBox<String> comboLocal;
    private JLabel labelCodBar;
    private JLabel labelNome;
    private JLabel labelEndDate;
    private JLabel labelLocal;
    private JLabel labelTitulo;
    private JScrollPane scrollPane;
    private JTable table;
    private JTextField txtCodBar;
    private JTextField txtNome;
    private JTextField txtEndDate;
    private JTextField txtStatus;
    private DefaultTableModel modelTable;
    // End of variables declaration                   

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie buttonAdd*/
    public JButton getButtonAdd(){
        return this.buttonAdd;
    }//end method getButtonAdd

    /**SET Method Propertie buttonAdd*/
    public void setButtonAdd(JButton buttonAdd){
        this.buttonAdd = buttonAdd;
    }//end method setButtonAdd

    /**GET Method Propertie buttonDelete*/
    public JButton getButtonDelete(){
        return this.buttonDelete;
    }//end method getButtonDelete

    /**SET Method Propertie buttonDelete*/
    public void setButtonDelete(JButton buttonDelete){
        this.buttonDelete = buttonDelete;
    }//end method setButtonDelete

    /**GET Method Propertie buttonCancel*/
    public JButton getButtonCancel(){
        return this.buttonCancel;
    }//end method getButtonCancel

    /**SET Method Propertie buttonCancel*/
    public void setButtonCancel(JButton buttonCancel){
        this.buttonCancel = buttonCancel;
    }//end method setButtonCancel

    /**GET Method Propertie buttonEdit*/
    public JButton getButtonEdit(){
        return this.buttonEdit;
    }//end method getButtonEdit

    /**SET Method Propertie buttonEdit*/
    public void setButtonEdit(JButton buttonEdit){
        this.buttonEdit = buttonEdit;
    }//end method setButtonEdit

    /**GET Method Propertie buttonPesquisa*/
    public JButton getButtonPesquisa(){
        return this.buttonPesquisa;
    }//end method getButtonPesquisa

    /**SET Method Propertie buttonPesquisa*/
    public void setButtonPesquisa(JButton buttonPesquisa){
        this.buttonPesquisa = buttonPesquisa;
    }//end method setButtonPesquisa

    /**GET Method Propertie comboLocal*/
    public javax.swing.JComboBox<java.lang.String> getComboLocal(){
        return this.comboLocal;
    }//end method getComboLocal

    /**SET Method Propertie comboLocal*/
    public void setComboLocal(javax.swing.JComboBox<java.lang.String> comboLocal){
        this.comboLocal = comboLocal;
    }//end method setComboLocal

    /**GET Method Propertie labelCodBar*/
    public JLabel getLabelCodBar(){
        return this.labelCodBar;
    }//end method getLabelCodBar

    /**SET Method Propertie labelCodBar*/
    public void setLabelCodBar(JLabel labelCodBar){
        this.labelCodBar = labelCodBar;
    }//end method setLabelCodBar

    /**GET Method Propertie labelNome*/
    public JLabel getLabelNome(){
        return this.labelNome;
    }//end method getLabelNome

    /**SET Method Propertie labelNome*/
    public void setLabelNome(JLabel labelNome){
        this.labelNome = labelNome;
    }//end method setLabelNome

    /**GET Method Propertie labelEndDate*/
    public JLabel getLabelEndDate(){
        return this.labelEndDate;
    }//end method getLabelEndDate

    /**SET Method Propertie labelEndDate*/
    public void setLabelEndDate(JLabel labelEndDate){
        this.labelEndDate = labelEndDate;
    }//end method setLabelEndDate

    /**GET Method Propertie labelLocal*/
    public JLabel getLabelLocal(){
        return this.labelLocal;
    }//end method getLabelLocal

    /**SET Method Propertie labelLocal*/
    public void setLabelLocal(JLabel labelLocal){
        this.labelLocal = labelLocal;
    }//end method setLabelLocal

    /**GET Method Propertie labelTitulo*/
    public JLabel getLabelTitulo(){
        return this.labelTitulo;
    }//end method getLabelTitulo

    /**SET Method Propertie labelTitulo*/
    public void setLabelTitulo(JLabel labelTitulo){
        this.labelTitulo = labelTitulo;
    }//end method setLabelTitulo

    /**GET Method Propertie scrollPane*/
    public JScrollPane getScrollPane(){
        return this.scrollPane;
    }//end method getScrollPane

    /**SET Method Propertie scrollPane*/
    public void setScrollPane(JScrollPane scrollPane){
        this.scrollPane = scrollPane;
    }//end method setScrollPane

    /**GET Method Propertie table*/
    public JTable getTable(){
        return this.table;
    }//end method getTable

    /**SET Method Propertie table*/
    public void setTable(JTable table){
        this.table = table;
    }//end method setTable

    /**GET Method Propertie txtCodBar*/
    public JTextField getTxtCodBar(){
        return this.txtCodBar;
    }//end method getTxtCodBar

    /**SET Method Propertie txtCodBar*/
    public void setTxtCodBar(JTextField txtCodBar){
        this.txtCodBar = txtCodBar;
    }//end method setTxtCodBar

    /**GET Method Propertie txtNome*/
    public JTextField getTxtNome(){
        return this.txtNome;
    }//end method getTxtNome

    /**SET Method Propertie txtNome*/
    public void setTxtNome(JTextField txtNome){
        this.txtNome = txtNome;
    }//end method setTxtNome

    /**GET Method Propertie txtEndDate*/
    public JTextField getTxtEndDate(){
        return this.txtEndDate;
    }//end method getTxtEndDate

    /**SET Method Propertie txtEndDate*/
    public void setTxtEndDate(JTextField txtEndDate){
        this.txtEndDate = txtEndDate;
    }//end method setTxtEndDate

    /**GET Method Propertie txtStatus*/
    public JTextField getTxtStatus(){
        return this.txtStatus;
    }//end method getTxtStatus

    /**SET Method Propertie txtStatus*/
    public void setTxtStatus(JTextField txtStatus){
        this.txtStatus = txtStatus;
    }//end method setTxtStatus

    //End GetterSetterExtension Source Code
    //!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie modelTable*/
    public DefaultTableModel getModelTable(){
        return this.modelTable;
    }//end method getModelTable

    //End GetterSetterExtension Source Code
    //!

    //Start GetterSetterExtension Source Code
    /**SET Method Propertie modelTable*/
    public void setModelTable(DefaultTableModel modelTable){
        this.modelTable = modelTable;
    }//end method setModelTable

    //End GetterSetterExtension Source Code

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie buttonConfirm*/
    public JButton getButtonConfirm(){
        return this.buttonConfirm;
    }//end method getButtonConfirm

    /**SET Method Propertie buttonConfirm*/
    public void setButtonConfirm(JButton buttonConfirm){
        this.buttonConfirm = buttonConfirm;
    }//end method setButtonConfirm

    //End GetterSetterExtension Source Code
    //!

    //Start GetterSetterExtension Source Code
    //End GetterSetterExtension Source Code
    //!
}
