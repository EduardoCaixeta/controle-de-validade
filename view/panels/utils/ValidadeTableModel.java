
package view.panels.utils;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.entity.Validade;
import model.Database;
import controller.ValidadeController;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class ValidadeTableModel extends AbstractTableModel implements TratamentoDate {

    //aqui transformei em coluna cada propriedade de Validade
    //que eu quero que seja exibida na tabela  
    private String colunas[] = {"Cod Barras", "Nome", "Local", "Validade"};
    private List<Validade> validades;
    private final int COLUNA_CODBAR = 0;
    private final int COLUNA_NOME = 1;
    private final int COLUNA_LOCAL = 2;
    private final int COLUNA_ENDDATE = 3;
    int rows;
    public ValidadeTableModel() {
        try
        {
            validades = new ValidadeController(new Database("Validade.db")).readAll();   
            ordene();
            rows = this.validades.size();
        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());   
        }
    }

    //retorna se a célula é editável ou não
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    //retorna o total de itens(que virarão linhas) da nossa lista
    @Override
    public int getRowCount() {
        return validades.size();
    }
    //retorna o total de colunas da tabela
    @Override
    public int getColumnCount() {
        return colunas.length;
    }
    //retorna o nome da coluna de acordo com seu indice
    @Override
    public String getColumnName(int indice) {
        return colunas[indice];
    }

    //determina o tipo de dado da coluna conforme seu indice
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case COLUNA_CODBAR:
            return String.class;
            case COLUNA_NOME:
            return String.class;
            case COLUNA_LOCAL:
            return String.class;
            case COLUNA_ENDDATE:
            return String.class;
            default:
            return String.class;
        }
    }

    //preenche cada célula da tabela
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Validade validade = this.validades.get(rowIndex);

        switch (columnIndex) {
            case COLUNA_CODBAR:
            return validade.getCodBar();
            case COLUNA_NOME:
            return "  "+validade.getNome();
            case COLUNA_LOCAL:
            return validade.getLocal();
            case COLUNA_ENDDATE:
            return validade.printEndDate();
        }
        return null;
    }
    //altera o valor do objeto de acordo com a célula editada
    //e notifica a alteração da tabela, para que ela seja atualizada na tela

    public void setValidades( List<Validade> validades)
    {
        int atualRows = this.validades.size();
        this.validades = validades;
        ordene();
        fireTableRowsUpdated(0, validades.size());
        if(validades.size() < rows)
        {
            fireTableRowsDeleted(validades.size(),rows);
        }
        else
        {
            fireTableRowsInserted(rows,validades.size());
        }
    }

    public void updateValidade(Validade upValidade)
    {
        int index = validades.indexOf(upValidade);
        validades.set(index,upValidade);
        ordene();
        fireTableRowsUpdated(0, validades.size());
    }
    
    public void ordene()
    {
        try
        {
            Validade aux = new Validade();
            int datePassada;
            for(int i = 0; i < validades.size() - 1; i++)
            {
                for(int j = i+1; j < validades.size(); j++)
                {
                    int diff = diffInDays(validades.get(i).printEndDate(), validades.get(j).printEndDate());
                    if(diff < 0)
                    {
                        

                        aux = validades.get(j);
                        validades.set(j, validades.get(i));
                        validades.set(i, aux);
                    }
                }
            }
        }
        catch(Exception e)
        {
            System.err.println("deuerrado");
        }
    } 

    public int diffInDays(String d1, String d2) throws Exception
    {
        try
        {
            int MILLIS_IN_DAY = 86400000;

            Calendar c1 = Calendar.getInstance();
            c1.setTime(stringToDate(d1));
            c1.set(Calendar.MILLISECOND, 0);
            c1.add(Calendar.MONTH, -1);
            c1.set(Calendar.SECOND, 0);
            c1.set(Calendar.MINUTE, 0);
            c1.set(Calendar.HOUR_OF_DAY, 0);

            Calendar c2 = Calendar.getInstance();
            c2.setTime(stringToDate(d2));
            c2.set(Calendar.MILLISECOND, 0);
            c2.add(Calendar.MONTH, -1);
            c2.set(Calendar.SECOND, 0);
            c2.set(Calendar.MINUTE, 0);
            c2.set(Calendar.HOUR_OF_DAY, 0);
            int diff = (int) ((c2.getTimeInMillis() - c1.getTimeInMillis()) / MILLIS_IN_DAY);

            return diff;
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    } 

    public Date stringToDate(String string ) throws Exception
    {
        try
        {
            Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
            Matcher matcher = dataPadrao.matcher(string);
            if (matcher.matches()) {
                int dia = Integer.parseInt(matcher.group(1));
                int mes = Integer.parseInt(matcher.group(2));
                int ano = Integer.parseInt(matcher.group(3));
                return (new GregorianCalendar(ano,mes,dia)).getTime();
            }
            else throw new Exception(" Data final inválida."); 
        }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }
    } 

    public void removeValidade(Validade validadeRemove)
    {
        validades.remove(validadeRemove);
            fireTableRowsDeleted(validades.size()+1,validades.size()+1);
            
    }
    
    public List<Validade> getValidades()
    {
        return this.validades;
    }
    
    public void analiseEndDate(String text){}
}
