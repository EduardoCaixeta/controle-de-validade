package view.panels.utils;

import javax.swing.JTextField;
import javax.swing.text.*;
 public class UpperCaseField_Limitado extends JTextField {
 
     private int quantidadeMax;
     private String bloq;
     public UpperCaseField_Limitado (int maxLen, String bloq)
        {
            super();
            quantidadeMax = maxLen;
            this.bloq = bloq;
        }
 
     protected Document createDefaultModel() {
         return new UpperCaseDocument();
     }
 
     public class UpperCaseDocument extends PlainDocument {
        
        
        @Override
        public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException
        {
            int totalquantia = (getLength() + str.length());
            if(totalquantia <= quantidadeMax)
            {
                if (str == null) 
                {
                       return;
                }
                char[] upper = str.toCharArray();
                for (int i = 0; i < upper.length; i++) 
                {
                    upper[i] = Character.toUpperCase(upper[i]);
                }
                super.insertString(offset, new String(upper).replaceAll(bloq,""), attr);
            }
        }
     }
 }
