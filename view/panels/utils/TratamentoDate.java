package view.panels.utils;


/**
 * Escreva a descrição da interface TratamentoDate aqui.
 * 
 * @author (seu nome) 
 * @version (número da versão ou data)
 */
import java.util.Date;

public interface TratamentoDate
{
    public abstract int diffInDays(String d1, String d2) throws Exception;
    
    public abstract Date stringToDate(String string ) throws Exception;
     public void analiseEndDate(String text) throws Exception;
    
}
