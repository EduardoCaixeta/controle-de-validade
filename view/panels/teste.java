package view.panels;

import model.*;
import model.entity.*;
import controller.*;
import javax.swing.*;
import java.util.*;
public class teste
{
    public static void main(String args[])
    {
        ViewProdutos panel = new ViewProdutos();
        ProdutosController ctr = new ProdutosController(new Database("Produtos.db"));
        List<Produtos> lista = new ArrayList<Produtos>();
        try
        {
           
            lista = ctr.readAll();   
        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
        // panel.getModel().insertRow(0,new Object[]{lista.get(0).getId(),lista.get(0).getProduto().getCodBar(),
         // lista.get(0).getProduto().getNome(),lista.get(0).getLocal()});
        
         for(int i = 0 ; i<lista.size(); i++)
         {
             panel.getModel().insertRow(i,new Object[]{lista.get(i).getId(),lista.get(i).getCodBar(),
                 lista.get(i).getNome(),lista.get(i).getLocal()});
         }
         
         JFrame frame = new JFrame();
         frame.add(panel);
         frame.pack();
         frame.setVisible(true);
    }
}
