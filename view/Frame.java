package view;
    
    import javax.swing.WindowConstants;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import view.panels.*;
import javax.swing.UIManager;
import javax.swing.LayoutStyle;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.Color;
import java.awt.Graphics;
import view.forms.*;
import model.Database;
public class Frame extends JFrame {

    /**
     * Creates new form Frame
     */
    public Frame() {
        menuBarra = new MenuBarra();
        menuBarra.getButtonCadastrar().addActionListener
        (e -> setCadastro());
        setSize(650,530);
         menuBarra.getButtonConsulta().addActionListener( e ->
        setConsulta());
        setForm(new FormConsulta(new Database("Produtos.db"), new Database("Validade.db"), this).getPanel());
        initComponents();
    }

    public void setCadastro()
    {
        getContentPane().removeAll();
        setForm(new FormCadastro(new Database("Produtos.db"), new Database("Validade.db"), this).getPanel());
        initComponents();
    }
    
    public void setConsulta()
    {
        getContentPane().removeAll();
        setForm(new FormConsulta(new Database("Produtos.db"), new Database("Validade.db"), this).getPanel());
        initComponents();
    }
    
    public void paintComponents(Graphics g)
    {
        g.setColor(new Color(255, 255, 255));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
    }
    
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(menuBarra, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(form, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(menuBarra, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(form, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleParent(this);

        pack();
    }// </editor-fold>                        

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        new Frame().setVisible(true);
    }

    // Variables declaration - do not modify                     
    private MenuBarra menuBarra;
    private JPanel form;

    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie menuBarra*/
    public MenuBarra getMenuBarra(){
        return this.menuBarra;
    }//end method getMenuBarra

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**SET Method Propertie form*/
    public void setForm(JPanel form){
        this.form = form;
        initComponents();
        this.validate();
    }//end method setForm

    //End GetterSetterExtension Source Code
//!
}
